package com.company;

/**
 * Created by Max on 17.07.2017.
 */
public class Orange extends Fruit {

    public Orange() {
        super(WEIGHT_ORANGE);
    }
}
