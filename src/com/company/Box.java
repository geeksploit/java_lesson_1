package com.company;

import java.util.ArrayList;

/**
 * Created by Max on 17.07.2017.
 */
public class Box<T extends Fruit> {

    private final float WEIGHT_PRECISION = 1e-3f;
    private ArrayList<T> contents = new ArrayList<>();

    /**
     * Adds one item to the box.
     *
     * @param item the item to be added
     */
    public void add(T item) {
        contents.add(item);
    }

    /**
     * Returns the current box weight.
     *
     * @return the box weight
     */
    public float getWeight() {
        int size = contents.size();
        if (size == 0)
            return 0;
        else
            return size * contents.get(0).getWeight();
    }

    /**
     * Compares the current box weight with a given box. Weights can be compared regardless of the boxes' contents.
     *
     * @param box a box to compare with
     * @return {@code true} if both boxes have the same weight within WEIGHT_PRECISION, {@code false} otherwise
     */
    public boolean compare(Box<?> box) {
        return Math.abs(this.getWeight() - box.getWeight()) < WEIGHT_PRECISION;
    }

    /**
     * Empty the current box into another one of the same kind.
     *
     * @param box the box to empty into
     */
    public void empty(Box<T> box) {
        if (box == this)
            return;
        for (T item : contents)
            box.add(item);
        contents.removeAll(contents);
    }
}
