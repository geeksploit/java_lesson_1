package com.company;

/**
 * Created by Max on 17.07.2017.
 */
public class Apple extends Fruit {

    public Apple() {
        super(WEIGHT_APPLE);
    }
}
