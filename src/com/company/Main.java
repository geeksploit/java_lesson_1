package com.company;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        // Task 1 showcase.
        System.out.println("Task 1");
        int[] arrPrimitive = {1, 2, 3};
        // swapTwoElements(arrPrimitive, 0, 2);      // Wrong 1st argument type.
        Object[] arrReference = {1, 2, 3};
        System.out.println("arrReference = " + Arrays.toString(arrReference));
        swapTwoElements(arrReference, 0, 2);
        System.out.println("arrReference = " + Arrays.toString(arrReference));

        // Task 2 showcase.
        System.out.println("\nTask 2");
        System.out.println("arrPrimitive = " + Arrays.toString(arrReference) + " " + arrReference.getClass().getSimpleName());
        ArrayList arrList = convertIntoArrayList(arrReference);
        System.out.println("lstPrimitive = " + arrList + " " + arrList.getClass().getSimpleName());

        // Task 3 showcase.
        System.out.println("\nTask 3");
        Box<Apple> boxApple = new Box<>();
        Box<Orange> boxOrange = new Box<>();

        // Test if box weight is measured properly.
        System.out.println("Weight test");
        System.out.println("boxApple = " + boxApple.getWeight());
        boxApple.add(new Apple());
        boxApple.add(new Apple());
        boxApple.add(new Apple());
        System.out.println("boxApple = " + boxApple.getWeight());
        System.out.println("boxOrange = " + boxOrange.getWeight());
        boxOrange.add(new Orange());
        System.out.println("boxOrange = " + boxOrange.getWeight());

        // Test if box comparison is working properly.
        System.out.println("\nComparison test");
        System.out.println("boxApple = " + boxApple.getWeight());
        System.out.println("boxOrange = " + boxOrange.getWeight());
        System.out.println("boxApple vs. boxOrange = " + boxApple.compare(boxOrange));
        boxOrange.add(new Orange());
        System.out.println("boxOrange = " + boxOrange.getWeight());
        System.out.println("boxApple vs. boxOrange = " + boxApple.compare(boxOrange));

        // Test if it is possible to empty a box into another one.
        System.out.println("\nEmptying test");
        Box<Orange> boxOrange2 = new Box<>();
        System.out.println("boxOrange = " + boxOrange.getWeight());
        System.out.println("boxOrange2 = " + boxOrange2.getWeight());
//        boxOrange.empty(boxApple);      // cannot be applied to Box<Apple>
        boxOrange.empty(boxOrange2);
        System.out.println("boxOrange = " + boxOrange.getWeight());
        System.out.println("boxOrange2 = " + boxOrange2.getWeight());
    }

    /**
     * Swaps two elements of the given array.
     *
     * @param arr the array in which to swap the elements
     * @param a the index of the first element to be swapped
     * @param b the index of the second element to be swapped
     * @param <T> the array's type
     */
    static <T> void swapTwoElements (T[] arr, int a, int b) {
        if (a == b)
            return;
        T tmp = arr[a];
        arr[a] = arr[b];
        arr[b] = tmp;
    }

    /**
     * Makes an ArrayList out of the given array.
     *
     * @param arr the array to be converted into ArrayList
     * @param <T> the class of the ArrayList elements
     * @return
     */
    static <T> ArrayList<T> convertIntoArrayList(T[] arr) {
        ArrayList<T> out = new ArrayList<>();
        for (T tmp : arr) {
            out.add(tmp);
        }
        return out;
    }
}
