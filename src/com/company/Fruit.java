package com.company;

/**
 * Created by Max on 17.07.2017.
 */
public class Fruit {
    protected static final float WEIGHT_APPLE = 1.0f;
    protected static final float WEIGHT_ORANGE = 1.5f;

    private float weight;

    protected Fruit(float weight) {
        this.weight = weight;
    }

    public float getWeight() {
        return this.weight;
    }
}
